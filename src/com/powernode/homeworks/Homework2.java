package com.powernode.homeworks;

/**
 * 描述 佛祖保佑,永无bug
 * 作者 周方哲
 * 时间 2023/10/24 19:58
 */
abstract class Printer{
    abstract void print();
}
class DoMatriPrinter extends Printer{
    @Override
    void print() {
        System.out.println("打印速度慢，效果差，噪音高");
    }
}
class InkperPrinter extends Printer{
    @Override
    void print() {
        System.out.println("打印效果介于针式和激光打印机之间");
    }
}
class LasterPrinter extends Printer{
    @Override
    void print() {
        System.out.println("大于速度快，噪音小，效果好");
    }
}
public class Homework2 {
    public static void main(String[] args) {
        DoMatriPrinter doMatriPrinter=new DoMatriPrinter();
        doMatriPrinter.print();
        InkperPrinter inkperPrinter=new InkperPrinter();
        inkperPrinter.print();
        LasterPrinter lasterPrinter=new LasterPrinter();
        lasterPrinter.print();
    }
}

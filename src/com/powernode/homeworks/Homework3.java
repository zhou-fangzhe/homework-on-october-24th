package com.powernode.homeworks;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 描述 佛祖保佑,永无bug
 * 作者 周方哲
 * 时间 2023/10/24 20:07
 */
class DateUtil{
    String s;

    public DateUtil() {
    }

    public DateUtil(String s) {
        this.s = s;
    }
    String dataTString(Date date) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String s=sdf.format(date);
        return s;
    }
    Date StringToData(String s){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date date=null;
        try {
            date=sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
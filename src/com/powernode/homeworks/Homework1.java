package com.powernode.homeworks;

import java.io.*;

/**
 * 描述 佛祖保佑,永无bug
 * 作者 周方哲
 * 时间 2023/10/24 19:02
 */
public class Homework1 {
    public static void main(String[] args) throws IOException {
        FileReader fr=new FileReader(new File("D:\\1.txt"));
        FileWriter fw=new FileWriter(new File("D:\\2.txt"));
        int len=-1;
        char[]car=new char[1024];
        while ((len= fr.read(car))!=-1){
            fw.write(car,0,len);
        }
        fr.close();
        fw.close();
        BufferedReader br=new BufferedReader(new FileReader("D:\\1.txt"));
        BufferedWriter bw=new BufferedWriter(new FileWriter("D:\\3.txt"));
        String line=null;
        int count=0;
        while ((line= br.readLine())!=null){
            if (line.contains("a")){
                System.out.println(line);
                count++;
            }
            bw.write(line);
            bw.newLine();
        }
        System.out.println("包含“a”字符串的个数为:"+count);
        br.close();
        bw.close();
    }
}
